﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;


namespace TelaVendas
{
    public class Vendas
    {
        // Método para realizar  testes na tela de conferência de vendas.
        public static void TestarConferenciaDeVendas(IWebDriver driver)
        {
            try
            {
                // Aguarda até que a página esteja completamente carregada antes de interagir com os elementos

                WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(8);
                IWebElement vendas = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[2]/a/i")));
                vendas.Click();

                // Navega até a tela de conferência de vendas

                IWebElement ConferenciadeVendas = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[2]/div/ul/li[1]/a/span")));
                ConferenciadeVendas.Click();

                // Selecionar uma empresa e aplicar filtros

                Actions.TestarRefo2(driver);


                //Testes aplicaveis a essa tela
                IWebElement selectAData = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"calendarioVendas\"]/div[2]/a[2]/div")));
                selectAData.Click();
                Thread.Sleep(1000);
                IWebElement selectAYear = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"calendarioVendas\"]/div[2]/a[1]/div/i")));
                selectAYear.Click();
                Thread.Sleep(1000);
                IWebElement selectNovember = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"calendarioVendas\"]/div[1]/div/div[1]/table/tbody/tr[3]/td[3]/span")));
                selectNovember.Click();
                Thread.Sleep(1000);
                IWebElement selectADay = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"calendarioVendas\"]/div[1]/div/div[1]/table/tbody/tr[3]/td[2]/span")));
                selectADay.Click();
                Thread.Sleep(10000);



            }
            // Em caso de erro, captura uma screenshot da tela e enviar para a pasta de erros.
            catch (NoSuchElementException)
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\NUnit\\Teste_Automatico_NUnit\\Teste_Automatico_NUnit\\Erros\\Nao_foi_possivel_encontrar_o_elemento_Tela_Conferencia_de_vendas_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);
                throw;
            }

        }

        // Método para realizar  testes na tela de vendas de sistemas .
        public static void TestarVendasistemas(IWebDriver driver)
        {
            try
            {
                // Aguarda até que a página esteja completamente carregada antes de interagir com os elementos.

                var waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(50));
                waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
                IWebElement vendas = waitjavascript.Until(ExpectedConditions.ElementToBeClickable((By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[2]/a/i"))));
                vendas.Click();

                // Navega até a tela de vendas sistemas.

                IWebElement vendasSistema = waitjavascript.Until(ExpectedConditions.ElementToBeClickable((By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[2]/div/ul/li[2]/a/span"))));
                vendasSistema.Click();

                // Selecionar uma empresa e aplicar filtros

                Actions.TestarRefo(driver);

                Actions.TestarSelecionaCalendario(driver);

                //Testes aplicaveis a essa tela.
                Thread.Sleep(120000);
                IWebElement export = waitjavascript.Until(ExpectedConditions.ElementToBeClickable((By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[5]/div/div/div/i"))));
                export.Click();
                Thread.Sleep(120000);

                IWebElement exportCSV = waitjavascript.Until(ExpectedConditions.ElementToBeClickable((By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[4]/div/div/div/i"))));
                exportCSV.Click();
            }
            // Em caso de erro, captura uma screenshot da tela e enviar para a pasta de erros.

            catch
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\NUnit\\Teste_Automatico_NUnit\\Teste_Automatico_NUnit\\Erros\\Error_na_Tela_Vendas_sistemas_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);
                throw;
            }
        }
        // Método para realizar  testes na tela de vendas operadoras .
        public static void TestarVendasoperadoras(IWebDriver driver)
        {
            try
            {
                // Aguarda até que a página esteja completamente carregada antes de interagir com os elementos
                var waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

                IWebElement vendas = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[2]/a/i")));
                vendas.Click();
                IWebElement vendasoperadoras = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[2]/div/ul/li[3]/a/span")));
                vendasoperadoras.Click();

                // Selecionar REFO
                Actions.TestarRefo(driver);

                Actions.TestarSelecionaCalendario(driver);
                Thread.Sleep(6000);
                IWebElement vendasBrutas = driver.FindElement(By.CssSelector("#VendasBrutas > div > div.kt-widget24__details.cardDetails"));
                vendasBrutas.Click();
                Thread.Sleep(4000);
                IWebElement taxa = driver.FindElement(By.CssSelector("#Taxa > div > div.kt-widget24__details.cardDetails"));
                taxa.Click();
                Thread.Sleep(4000);
                IWebElement vendasLíquidas = driver.FindElement(By.CssSelector("#VendasLiquidas > div > div.kt-widget24__action"));
                vendasLíquidas.Click();
                Thread.Sleep(4000);
                IWebElement débitos = driver.FindElement(By.CssSelector("#Debitos > div > div.kt-widget24__details.cardDetails"));
                débitos.Click();
                Thread.Sleep(4000);
                IWebElement rejeitados = driver.FindElement(By.CssSelector("#Rejeitados > div > div.kt-widget24__details.cardDetails"));
                rejeitados.Click();
                Thread.Sleep(4000);
                IWebElement totalLiquido = driver.FindElement(By.CssSelector("#TotalLiquido > div > div.kt-widget24__details.cardDetails"));
                totalLiquido.Click();
                Thread.Sleep(10000);
                IWebElement Exportartodososdados = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[7]/div/div/div")));
                Exportartodososdados.Click();
                Thread.Sleep(10000);
                IWebElement ExportarPDF = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[6]/div/div/div/i")));
                ExportarPDF.Click();
                Thread.Sleep(10000);
                IWebElement ExportarCSV = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[5]/div/div")));
                ExportarCSV.Click();
                Thread.Sleep(3000);

            }
            // Em caso de erro, captura uma screenshot da tela e enviar para a pasta de erros.

            catch
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\NUnit\\Teste_Automatico_NUnit\\Teste_Automatico_NUnit\\Erros\\Error_na_Tela_Vendas_Operadoras_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);
                throw;
            }
        }
    }
}
