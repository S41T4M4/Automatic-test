
Este é um teste automático desenvolvido para verificar o funcionamento correto de um site através da interação com suas telas utilizando Selenium WebDriver em conjunto com NUnit para execução e assertivas.


Erro_nas_telas: Esta pasta conterá os prints capturados em caso de erro durante a execução do teste.
Classes para Telas: Cada tela do site possui uma classe dedicada, contendo métodos para interagir com os elementos específicos dessa tela.
Ações em Elementos: Dentro de cada classe de tela, há métodos para realizar ações nos elementos daquela tela, como clicar em botões e interagir com diversas partes da tela.
Pasta para Erros: Os prints capturados durante a execução do teste serão armazenados nesta pasta para referência e análise.