using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using NUnit_Teste_Automatico.Telas;
using TelaBancária;

[TestFixture]
public class Testes
{
    IWebDriver driver;

    [SetUp]
    public void Inicializar()
    {
        driver = new ChromeDriver();

    }

    [TearDown]
    public void Finalizar()
    {
        Actions.TestarDeslogar(driver);
        driver.Quit();
    }

    [Test]
    public void TesteCompleto()
    {
        // Acessar o link da conciliadora, maximizar a tela e aguardar para colocar o usuario e senha;

        // Colocar email e senha e ir para a tela da conciliadora.
        Actions.TestarLogin(driver);

        // Entrar na tela de Dashboard Gerencial e fazer validações:
        Dashboard.TestarDashboardGerencial(driver);

        // Entrar na tela de Vendas e fazer validações:

        Vendas.TestarConferenciaDeVendas(driver);
        //Vendas.TestarVendasistemas(driver);
        Vendas.TestarVendasoperadoras(driver);

        // Entrar na tela de Pagamentos e fazer validações: 

        Pagamentos.TestarPagamentosConfirmados(driver);
        Pagamentos.TestarPrevisãoderecebimentosSinteticos(driver);
        Pagamentos.TestarPagamentosPorContaBancária(driver);
        Pagamentos.TestarRelatoriodeBaixas(driver);

        // Entrar na tela de Taxa e fazer validações: 
        Taxas.TestarCadastrodeTaxa(driver);
        Taxas.TestarRelatóriodeTaxa(driver);

        // Entrar na tela Banco e fazer validações:

        Banco.TestarConciliaçãobancária(driver);

    }
}
