﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System.Threading;
using System;

namespace NUnit_Teste_Automatico.Telas
{
    public class Taxas
    {
        public static void TestarCadastrodeTaxa(IWebDriver driver)
        {
            try
            {
                // Aguarda até que a página esteja completamente carregada antes de interagir com os elementos

                var waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                waitjavascript.Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

                IWebElement taxa = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[4]/a/i")));
                taxa.Click();
                IWebElement cadastrodetaxa = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[4]/div/ul/li[1]/a/span")));
                cadastrodetaxa.Click();

                // Selecionar uma empresa e aplicar filtros
                Actions.TestarRefo2(driver);




                Thread.Sleep(10000);
                IWebElement exportartudo = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[7]/div/div/div/i")));
                exportartudo.Click();
                Thread.Sleep(10000);
                IWebElement exportarPDF = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[5]/div/div/div/i")));
                exportarPDF.Click();
                Thread.Sleep(10000);
                IWebElement exportarCSV = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[4]/div/div/div/i")));
                exportarCSV.Click();
            }
            // Em caso de erro, captura uma screenshot da tela e enviar para a pasta de erro
            catch
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\Teste Automatico\\NUnit_Teste_Automatico\\Erro_nas_Telas\\Erro_na_tela_Cadastro_de_Taxa_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);

                throw;
            }
        }
        public static void TestarRelatóriodeTaxa(IWebDriver driver)
        {
            try
            {
                // Aguarda até que a página esteja completamente carregada antes de interagir com os elementos

                var waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                waitjavascript.Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
                IWebElement taxa = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[4]/a/i")));
                taxa.Click();
                IWebElement relátorio = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[4]/div/ul/li[2]/a/span")));
                relátorio.Click();
                // Selecionar uma empresa e aplicar filtros
                Actions.TestarRefo(driver);

                // Ações na tela de Relatório de Taxa

                IWebElement exportartodososdados = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[5]/div/div/div/i")));
                exportartodososdados.Click();
                Thread.Sleep(4000);
                IWebElement exportar_csv = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[4]/div/div/div/i")));
                exportar_csv.Click();
            }
            // Em caso de erro, captura uma screenshot da tela e enviar para a pasta de erros.
            catch
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\Teste Automatico\\NUnit_Teste_Automatico\\Erro_nas_Telas\\Erro_na_tela_Relatorio_de_Taxa_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);
                throw;
            }
        }
    }
}
