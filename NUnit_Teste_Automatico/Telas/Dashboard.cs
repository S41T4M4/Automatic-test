﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System.Threading;
using System;

namespace NUnit_Teste_Automatico.Telas
{
    public class Dashboard
    {
        public static void TestarDashboardGerencial(IWebDriver driver)
        {
            try
            {
                // Aguarda até que a página esteja completamente carregada antes de interagir com os elementos
                WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                waitjavascript.Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                IWebElement selecionarDashboard = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[1]/a/i")));
                selecionarDashboard.Click();
                IWebElement SelecionarDashboardGerencial = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[1]/div/ul/li[1]/a/span")));
                SelecionarDashboardGerencial.Click();


                // Aplicando o REFO de testes
                Actions.TestarRefo(driver);

                Actions.TestarSelecionaCalendario(driver);

                // Executando ações na tela

                Interactions(driver);


            }
            // Em caso de erro, captura uma screenshot da tela e enviar para a pasta de erros.

            catch
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\Teste Automatico\\NUnit_Teste_Automatico\\Erro_nas_Telas\\Nao_foi_possivel_encontrar_o_elemento_Tela_Dashboard_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);
                throw;
            }
        }
        // Interações da tela Dashboard Gerencial :
        public static void Interactions(IWebDriver driver)
        {
            WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            waitjavascript.Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            IWebElement agrupamentoVendas = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"groupContainerVenda\"]/select")));
            agrupamentoVendas.Click();
            agrupamentoVendas.SendKeys(Keys.ArrowDown);
            agrupamentoVendas.SendKeys(Keys.Enter);
            Thread.Sleep(10000);
            agrupamentoVendas.Click();
            agrupamentoVendas.SendKeys(Keys.ArrowDown);
            agrupamentoVendas.SendKeys(Keys.Enter);
            Thread.Sleep(10000);
            agrupamentoVendas.Click();
            agrupamentoVendas.SendKeys(Keys.ArrowDown);
            agrupamentoVendas.SendKeys(Keys.Enter);
            Thread.Sleep(10000);
            agrupamentoVendas.Click();
            agrupamentoVendas.SendKeys(Keys.ArrowDown);
            agrupamentoVendas.SendKeys(Keys.Enter);
            Thread.Sleep(10000);
            agrupamentoVendas.Click();
            agrupamentoVendas.SendKeys(Keys.ArrowDown);
            agrupamentoVendas.SendKeys(Keys.Enter);
            Thread.Sleep(8000);


            IWebElement agrupamentoPagamentos = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"groupContainerPagto\"]/select")));
            agrupamentoPagamentos.Click();
            agrupamentoPagamentos.SendKeys(Keys.ArrowDown);
            agrupamentoPagamentos.SendKeys(Keys.Enter);
            Thread.Sleep(10000);
            agrupamentoPagamentos.Click();
            agrupamentoPagamentos.SendKeys(Keys.ArrowDown);
            agrupamentoPagamentos.SendKeys(Keys.Enter);
            Thread.Sleep(9000);
            agrupamentoPagamentos.Click();
            agrupamentoPagamentos.SendKeys(Keys.ArrowDown);
            agrupamentoPagamentos.SendKeys(Keys.Enter);
            Thread.Sleep(7000);
            agrupamentoPagamentos.Click();
            agrupamentoPagamentos.SendKeys(Keys.ArrowDown);
            agrupamentoPagamentos.SendKeys(Keys.Enter);
            Thread.Sleep(10000);
            agrupamentoPagamentos.Click();
            agrupamentoPagamentos.SendKeys(Keys.ArrowDown);
            agrupamentoPagamentos.SendKeys(Keys.Enter);
            Thread.Sleep(10000);
            agrupamentoPagamentos.Click();
            agrupamentoPagamentos.SendKeys(Keys.ArrowDown);
            agrupamentoPagamentos.SendKeys(Keys.Enter);
            Thread.Sleep(10000);
            agrupamentoPagamentos.Click();
            agrupamentoPagamentos.SendKeys(Keys.ArrowDown);
            agrupamentoPagamentos.SendKeys(Keys.Enter);
            Thread.Sleep(10000);


            IWebElement taxasAplicadas = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"groupContainerTaxa\"]/select")));
            taxasAplicadas.Click();
            taxasAplicadas.SendKeys(Keys.ArrowDown);
            taxasAplicadas.SendKeys(Keys.Enter);
            Thread.Sleep(10000);
            taxasAplicadas.Click();
            taxasAplicadas.SendKeys(Keys.ArrowDown);
            taxasAplicadas.SendKeys(Keys.Enter);
            Thread.Sleep(7000);
            taxasAplicadas.Click();
            taxasAplicadas.SendKeys(Keys.ArrowDown);
            taxasAplicadas.SendKeys(Keys.Enter);
            Thread.Sleep(10000);
            taxasAplicadas.Click();
            taxasAplicadas.SendKeys(Keys.ArrowDown);
            taxasAplicadas.SendKeys(Keys.Enter);
            Thread.Sleep(10000);


            IWebElement exportarPdfVendas = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridVendas\"]/div/div[4]/div/div/div[3]/div[1]/div/div/div/i")));
            IWebElement exportarPdfPagamentos = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPagamentos\"]/div/div[4]/div/div/div[3]/div[1]/div/div/div/i")));
            IWebElement exportarPdfTaxasAplicadas = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridTaxa\"]/div/div[4]/div/div/div[3]/div[1]/div/div/div/i")));
            exportarPdfVendas.Click();
            exportarPdfPagamentos.Click();
            exportarPdfTaxasAplicadas.Click();
        }
    }
}


