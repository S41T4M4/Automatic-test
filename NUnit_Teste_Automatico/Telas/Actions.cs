﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Reflection.Metadata;
using System.Threading;


public class Actions
{
    public static void TestarLogin(IWebDriver driver)
    {
        try
        {
            WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
            driver.Navigate().GoToUrl("https://intranet-hom.conciliadora.com.br/");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            IWebElement email = waitjavascript.Until(ExpectedConditions.ElementToBeClickable((By.CssSelector("#login"))));
            email.Click();
            email.SendKeys("testesautomatizados@conciliadora.com.br");

            IWebElement senha = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#password")));
            senha.Click();
            senha.SendKeys("Teste123");
            senha.SendKeys(Keys.Enter);

            Thread.Sleep(7000);
        }
        catch
        {
            Console.WriteLine("Erro ao logar: Não foi possivel encontrar o elemento");
            string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
            string screenshotPath = $"C:\\Users\\vitor.reis\\Teste Automatico\\NUnit_Teste_Automatico\\Erro_nas_Telas\\Erro_ao_Logar_{timestamp}.png";
            Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
            scs.SaveAsFile(screenshotPath);
            throw;
        }
    }
    public static void TestarDeslogar(IWebDriver driver)
    {
        try
        {
            WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(8);
            IWebElement deslogar = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_header\"]/div/div/div[3]/div[4]/div[1]/span/i")));
            deslogar.Click();

            IWebElement sair = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_header\"]/div/div/div[3]/div[4]/div[2]/div[2]/div/a/span")));
            sair.Click();

            Thread.Sleep(3000);
            IWebElement confirmar = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("/html/body/div[8]/div/div[3]/button[1]")));
            confirmar.Click();

        }
        catch
        {

            string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
            string screenshotPath = $"C:\\Users\\vitor.reis\\Teste Automatico\\NUnit_Teste_Automatico\\Erro_nas_Telas\\Erro_ao_Logar_{timestamp}.png";
            Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
            scs.SaveAsFile(screenshotPath);

            throw;
        }
    }
    public static void TestarRefo(IWebDriver driver)
    {
        var waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        try
        {

            IWebElement apagarEmpresaSelecionada = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"dropDownSearch\"]/div/div/div[1]/div[1]/div/div")));
            apagarEmpresaSelecionada.Click();
            Thread.Sleep(5000);
            IWebElement selecionarUmaEmpresa = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"dropDownSearch\"]/div[1]/div/div[1]/input")));
            selecionarUmaEmpresa.Click();
            selecionarUmaEmpresa.SendKeys("33292");
            Thread.Sleep(5000);
            selecionarUmaEmpresa.SendKeys(Keys.Enter);
            Thread.Sleep(5000);
            IWebElement botaoAplicar = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"btnApply\"]/span")));
            botaoAplicar.Click();
            Thread.Sleep(15000);
        }
        catch
        {
            IWebElement selecionarUmaEmpresa = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"dropDownSearch\"]/div[1]/div/div[1]/input")));
            selecionarUmaEmpresa.Click();
            selecionarUmaEmpresa.SendKeys("33292");
            Thread.Sleep(3000);
            selecionarUmaEmpresa.SendKeys(Keys.Enter);
            Thread.Sleep(5000);
            IWebElement botaoAplicar = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"btnApply\"]/span")));
            botaoAplicar.Click();
            Thread.Sleep(5000);
            throw;
        }
    }
    public static void TestarRefo2(IWebDriver driver)
    {
        var waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        try
        {

            IWebElement apagarEmpresaSelecionada = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"dropDownSearch\"]/div[1]/div/div[2]/span/span")));
            apagarEmpresaSelecionada.Click();
            Thread.Sleep(5000);
            IWebElement selecionarUmaEmpresa = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"dropDownSearch\"]/div[1]/div/div[1]/input")));
            selecionarUmaEmpresa.Click();
            selecionarUmaEmpresa.SendKeys("33292");
            Thread.Sleep(5000);
            selecionarUmaEmpresa.SendKeys(Keys.Enter);
            Thread.Sleep(5000);
            IWebElement botaoAplicar = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"btnApply\"]/span")));
            botaoAplicar.Click();
            Thread.Sleep(10000);
        }
        catch
        {
            IWebElement selecionarUmaEmpresa = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"dropDownSearch\"]/div[1]/div/div[1]/input")));
            selecionarUmaEmpresa.Click();
            selecionarUmaEmpresa.SendKeys("33292");
            Thread.Sleep(3000);
            selecionarUmaEmpresa.SendKeys(Keys.Enter);
            Thread.Sleep(5000);
            IWebElement botaoAplicar = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"btnApply\"]/span")));
            botaoAplicar.Click();
            Thread.Sleep(5000);
            throw;
        }
    }
    public static void TestarRefo3(IWebDriver driver)
    {
        var waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        try
        {

            IWebElement apagarEmpresaSelecionada = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"dropDownSearch\"]/div[1]/div/div[2]/span/span")));
            apagarEmpresaSelecionada.Click();
            Thread.Sleep(5000);
            IWebElement selecionarUmaEmpresa = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"dropDownSearch\"]/div[1]/div/div[1]/input")));
            selecionarUmaEmpresa.Click();
            selecionarUmaEmpresa.SendKeys("4738");
            Thread.Sleep(5000);
            selecionarUmaEmpresa.SendKeys(Keys.Enter);
            Thread.Sleep(5000);
            IWebElement botaoAplicar = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"btnApply\"]/span")));
            botaoAplicar.Click();

            Thread.Sleep(10000);
        }
        catch
        {
            IWebElement selecionarUmaEmpresa = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"dropDownSearch\"]/div[1]/div/div[1]/input")));
            selecionarUmaEmpresa.Click();
            selecionarUmaEmpresa.SendKeys("4738");
            Thread.Sleep(3000);
            selecionarUmaEmpresa.SendKeys(Keys.Enter);
            Thread.Sleep(5000);
            IWebElement botaoAplicar = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"btnApply\"]/span")));
            botaoAplicar.Click();
            Thread.Sleep(5000);
            throw;
        }
    }


    public static void TestarSelecionaCalendario(IWebDriver driver)
    {
        //Seleciona o calendario
        WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
        waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        IWebElement abrirCalendario = waitjavascript.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id=\"dateRangePicker\"]")));
        abrirCalendario.Click();
        Thread.Sleep(2000);

        IWebElement selecionaMes = waitjavascript.Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/div[7]/div[2]/div[1]/table/thead/tr[1]/th[2]/select[1]/option[11]")));
        selecionaMes.Click();
        Thread.Sleep(2000);

        IWebElement selecionaAno = waitjavascript.Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/div[7]/div[2]/div[1]/table/thead/tr[1]/th[2]/select[2]/option[14]")));
        selecionaAno.Click();
        Thread.Sleep(2000);

        IWebElement selecionaDia1 = waitjavascript.Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/div[7]/div[2]/div[1]/table/tbody/tr[3]/td[2]")));
        selecionaDia1.Click();
        Thread.Sleep(2000);

        IWebElement selecionaDia31 = waitjavascript.Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/div[7]/div[2]/div[1]/table/tbody/tr[3]/td[2]")));
        selecionaDia31.Click();
        Thread.Sleep(5000);

        IWebElement botaoAplicar = waitjavascript.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id=\"btnApply\"]")));
        botaoAplicar.Click();
        Thread.Sleep(10000);
    }
}

