﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System.Threading;
using System;

namespace TelaBancária
{
    public class Banco
    {
        public static void TestarConciliaçãobancária(IWebDriver driver)
        {
            try
            {
                // Aguarda até que a página esteja completamente carregada antes de interagir com os elementos
                WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                waitjavascript.Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
                IWebElement banco = waitjavascript.Until(ExpectedConditions.ElementExists(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[5]/a/i")));
                banco.Click();
                IWebElement conciliação_bancária = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[5]/div/ul/li/a/span")));
                conciliação_bancária.Click();
                Thread.Sleep(5000);
                // Selecionar uma empresa
                Actions.TestarRefo3(driver);

                Actions.TestarSelecionaCalendario(driver);

                IWebElement Upload = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"btnUploadBancaria\"]/span")));
                Upload.Click();
                Thread.Sleep(3000);

                IWebElement close = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"modal-header\"]/button")));
                close.Click();

            }
            // Em caso de erro, captura uma screenshot da tela e enviar para a pasta de erro
            catch
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\Teste Automatico\\NUnit_Teste_Automatico\\Erro_nas_Telas\\Erro_na_tela_Previsão_de_Conciliação_Bancaria_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);
                throw;
            }
        }
    }
}
