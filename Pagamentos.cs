﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System.Threading;
using System;

namespace TelaPagamentos
{
    public class Pagamentos
    {
        public static void TestarPagamentosConfirmados(IWebDriver driver)
        {
            try
            {

                // Aguarda até que a página esteja completamente carregada antes de interagir com os elementos

                WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
                waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                IWebElement pagamentos = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[3]/a/i")));
                pagamentos.Click();
                IWebElement pagamentosconfirmados = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[3]/div/ul/li[1]/a/span")));
                pagamentosconfirmados.Click();

                // Selecionar uma empresa e aplicar filtros
                Actions.TestarRefo(driver);

                Actions.TestarSelecionaCalendario(driver);
                Thread.Sleep(8000);

                IWebElement cardVendasBrutas = driver.FindElement(By.CssSelector("#VendasBrutas > div > div.kt-widget24__details.cardDetails"));
                cardVendasBrutas.Click();
                Thread.Sleep(7000);

                IWebElement Exportartodososdados = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[7]/div/div/div")));
                Exportartodososdados.Click();
                Thread.Sleep(6000);
                IWebElement ExportaremPDF = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[6]/div/div/div/i")));
                ExportaremPDF.Click();
                Thread.Sleep(6000);
                IWebElement ExportarCSV = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[5]/div/div")));
                ExportarCSV.Click();
            }
            // Em caso de erro, captura uma screenshot da tela e enviar para a pasta de erros.
            catch
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\NUnit\\Teste_Automatico_NUnit\\Teste_Automatico_NUnit\\Erros\\Erro_na_tela_Pagamentos_Confirmados_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);
                throw;
            }
        }
        public static void TestarPrevisãodeRecebimentos(IWebDriver driver)
        {
            try
            {
                WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
                IWebElement pagamentos = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[3]/a/i")));
                pagamentos.Click();
                IWebElement previsaoDeRecebimentos = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[3]/div/ul/li[2]/a/span")));
                previsaoDeRecebimentos.Click();
                // Selecionar uma empresa e aplicar filtros

                Actions.TestarRefo(driver);

                Actions.TestarSelecionaCalendario(driver);

                IWebElement exportarTodososDados = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[5]/div/div/div/i")));
                exportarTodososDados.Click();
                Thread.Sleep(10000);
                IWebElement exportarCsv = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[4]/div/div/div/i")));
                exportarCsv.Click();

            }
            catch
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\NUnit\\Teste_Automatico_NUnit\\Teste_Automatico_NUnit\\Erros\\Erro_na_tela_Previsão_de_recebimentos_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);
                throw;
            }
        }

        public static void TestarPrevisãoderecebimentosSinteticos(IWebDriver driver)
        {
            try
            {

                // Aguarda até que a página esteja completamente carregada antes de interagir com os elementos

                WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
                IWebElement pagamentos = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[3]/a/i")));
                pagamentos.Click();
                IWebElement previsãoDeRecebimentosSinteticos = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[3]/div/ul/li[3]/a/span")));
                previsãoDeRecebimentosSinteticos.Click();
                Thread.Sleep(20000);

                //Selecionar refo

                Actions.TestarRefo(driver);

                Actions.TestarSelecionaCalendario(driver);

            }
            // Em caso de erro, captura uma screenshot da tela e enviar para a pasta de erros.
            catch
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\NUnit\\Teste_Automatico_NUnit\\Teste_Automatico_NUnit\\Erros\\Erro_na_tela_Previsão_de_recebimentos_sinteticos_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);
                throw;
            }

        }
        public static void TestarPagamentosPorContaBancária(IWebDriver driver)
        {
            try
            {

                WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(8);

                IWebElement pagamentos = driver.FindElement(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[3]/a/i"));
                pagamentos.Click();
                IWebElement PagamentosporConta = driver.FindElement(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[3]/div/ul/li[5]/a/span"));
                PagamentosporConta.Click();
                Thread.Sleep(5000);


                //Selecionar refo

                Actions.TestarRefo(driver);

                Actions.TestarSelecionaCalendario(driver);

                Thread.Sleep(6000);
                IWebElement exportartudo = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[5]/div/div/div/i")));
                exportartudo.Click();
                Thread.Sleep(6000);
                IWebElement exportaremCSV = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[4]/div/div/div/i")));
                exportaremCSV.Click();
            }
            // Em caso de erro, captura uma screenshot da tela e enviar para a pasta de erros.
            catch
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\NUnit\\Teste_Automatico_NUnit\\Teste_Automatico_NUnit\\Erros\\Erro_na_tela_PagamentosPorContaBancária_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);
                throw;
            }
        }
        public static void TestarRelatoriodeBaixas(IWebDriver driver)
        {
            try
            {

                WebDriverWait waitjavascript = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                waitjavascript.Until(driver => (bool)((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

                IWebElement pagamentos = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[3]/a/i")));
                pagamentos.Click();
                IWebElement relaoriodeBaixas = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"kt_aside_menu\"]/ul/li[3]/div/ul/li[6]/a/span")));
                relaoriodeBaixas.Click();
                Thread.Sleep(5000);

                //Selecionar Refo

                Actions.TestarRefo(driver);

                Actions.TestarSelecionaCalendario(driver);

                IWebElement exportarTodososDados = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[5]/div/div/div/i")));
                exportarTodososDados.Click();
                Thread.Sleep(4000);
                IWebElement exportarCsv = waitjavascript.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"gridPrincipal\"]/div/div[4]/div/div/div[3]/div[4]/div/div/div/i")));
                exportarCsv.Click();

            }
            // Em caso de erro, captura uma screenshot da tela e enviar para a pasta de erros.
            catch
            {
                string timestamp = DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss");
                string screenshotPath = $"C:\\Users\\vitor.reis\\NUnit\\Teste_Automatico_NUnit\\Teste_Automatico_NUnit\\Erros\\Erro_na_tela_Relatorio_de_Baixas_{timestamp}.png";
                Screenshot scs = ((ITakesScreenshot)driver).GetScreenshot();
                scs.SaveAsFile(screenshotPath);
                throw;
            }
        }
    }
}
